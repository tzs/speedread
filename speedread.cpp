//#include <windows.h>
#include <stdio.h>
#include <iostream>
#include <string>

#include <unistd.h>
#include <signal.h>
#include <sys/time.h>

typedef unsigned long DWORD;

bool cont = true;

void
stop( int sig )
{
    cont = false;
}

void
Sleep( int msec )
{
    usleep( msec * 1000 );
}

DWORD
GetTickCount()
{
    static unsigned long base = 0;
    struct timeval tv;
    gettimeofday( &tv, 0 );
    if ( base == 0 )
        base = tv.tv_sec;
    return (tv.tv_sec - base) * 1000 + tv.tv_usec / 1000;
}

int main( int argc, char *argv[] )
{
    int delay = 250;
    std::string word;

    if ( argc == 2 )
        delay = atoi(argv[1]);

    signal( SIGINT, stop );

    std::cout << "\n\n\n";

    int words = 0;
    DWORD start = GetTickCount();
    std::string display_word = "";
    while ( std::cin >> word && cont )
    {
        Sleep(delay);
        ++words;
        if ( display_word.length() )
            display_word += ' ';
        display_word += word;
        //if ( display_word.length() < 5 )
            //continue;
        word = display_word;
        display_word = "";
        int punc = word.find(".");
        if ( punc == std::string::npos )
            punc = word.find(",");
        int pad = 15 - word.length()/2;
        for ( int i = 0; i < pad; ++i )
            std::cout << " ";
        std::cout << " " << word << "                                  \r";
        std::cout.flush();
        if ( punc != std::string::npos )
            Sleep(2*delay);
    }
    DWORD stop = GetTickCount();
    float seconds = (stop - start) / 1000.0;
    printf( "\n%d words in %f seconds\n", words, seconds );
    if ( seconds != 0 )
        printf( "%f words/second, %f words/minute\n", words/seconds,
                (60*words)/seconds );
    return 0;
}
